-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 06, 2023 at 05:06 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `edhuniv`
--

-- --------------------------------------------------------

--
-- Table structure for table `daftar_dosen`
--

CREATE TABLE `daftar_dosen` (
  `Id` int(11) NOT NULL,
  `Nip_Nidn` int(11) DEFAULT NULL,
  `Nama` varchar(99) DEFAULT NULL,
  `Jenis_kelamin` varchar(25) DEFAULT NULL,
  `Program_studi` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `daftar_dosen`
--

INSERT INTO `daftar_dosen` (`Id`, `Nip_Nidn`, `Nama`, `Jenis_kelamin`, `Program_studi`) VALUES
(224, 21001, 'Budi Mandala', 'Laki-laki', 'B.Inggris'),
(456, 20056, 'Nur Aziz ', 'Laki-laki', 'T.Informatika'),
(892, 10001, 'Anardani', 'Laki-laki', 'T.Informatika');

-- --------------------------------------------------------

--
-- Table structure for table `daftar_jadwal`
--

CREATE TABLE `daftar_jadwal` (
  `Id` int(12) NOT NULL,
  `Kode_matkul` char(20) DEFAULT NULL,
  `Tanggal` date DEFAULT NULL,
  `Waktu` time DEFAULT NULL,
  `Matakuliah` varchar(60) DEFAULT NULL,
  `Prodi` varchar(50) DEFAULT NULL,
  `Semester` char(15) DEFAULT NULL,
  `Kelas` char(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `daftar_jadwal`
--

INSERT INTO `daftar_jadwal` (`Id`, `Kode_matkul`, `Tanggal`, `Waktu`, `Matakuliah`, `Prodi`, `Semester`, `Kelas`) VALUES
(765, '2002', '0000-00-00', '13:01:00', 'Fisika', 'Farmasi', '3', 'D');

-- --------------------------------------------------------

--
-- Table structure for table `daftar_mahasiswa`
--

CREATE TABLE `daftar_mahasiswa` (
  `Id` int(12) NOT NULL,
  `Nim` varchar(20) DEFAULT NULL,
  `Nama` varchar(99) DEFAULT NULL,
  `Jenis_kelamin` varchar(30) DEFAULT NULL,
  `Prodi` varchar(20) DEFAULT NULL,
  `Semester` varchar(19) DEFAULT NULL,
  `Kelas` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `daftar_mahasiswa`
--

INSERT INTO `daftar_mahasiswa` (`Id`, `Nim`, `Nama`, `Jenis_kelamin`, `Prodi`, `Semester`, `Kelas`) VALUES
(222, '12122112', 'Dani', 'Laki-laki', 'Manajemen', '3', 'D'),
(671, '2005101078', 'Erfan Dedihandika H', 'Laki-laki', 'T.Informatika', '5', 'D');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`) VALUES
(8, 'Murahsenyum', 'murahsenyum@gmail.com', '202cb962ac59075b964b07152d234b70'),
(9, 'erfan', 'erfandika34@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b'),
(10, 'www', 'erfandedi0@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b');

-- --------------------------------------------------------

--
-- Table structure for table `user_form`
--

CREATE TABLE `user_form` (
  `id` int(12) NOT NULL,
  `name` varchar(99) DEFAULT NULL,
  `email` varchar(99) DEFAULT NULL,
  `Password` varchar(99) DEFAULT NULL,
  `user_type` varchar(99) DEFAULT 'user',
  `nim` int(12) DEFAULT NULL,
  `nama_mahasiswa` varchar(99) DEFAULT NULL,
  `tempat_tgl_lahir` varchar(50) DEFAULT NULL,
  `jenis_kelamin` varchar(50) DEFAULT NULL,
  `jurusan` varchar(50) DEFAULT NULL,
  `kurikulum` varchar(30) DEFAULT NULL,
  `sistem_kuliah` varchar(25) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_form`
--

INSERT INTO `user_form` (`id`, `name`, `email`, `Password`, `user_type`, `nim`, `nama_mahasiswa`, `tempat_tgl_lahir`, `jenis_kelamin`, `jurusan`, `kurikulum`, `sistem_kuliah`, `image`) VALUES
(15, 'admin', 'admin@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(71, 'erfan', 'erfan@gmail.com', '3dbe00a167653a1aaee01d93e77e730e', 'user', 2005101078, 'Erfan Dedihandika Hardiyansa', 'Madiun,22 Mret 2001', 'Laki-laki', 'T.Informatika', 'Merdekan', 'pagi', 'pemrograman-web.jpg'),
(72, 'aaa', 'aa@yahoo.com', '3dbe00a167653a1aaee01d93e77e730e', 'user', 1111, 'qsqqsqq', 'qqqq', 'wss', 'sw', 'aaaa', 'qeqe', 'rpl.png'),
(73, 'dea', 'dea12@gmail.com', '25d55ad283aa400af464c76d713c07ad', 'user', 2005101063, 'dea fidiyaningrum', 'magetan,14 desember 2001', 'perempuan', 'T.Informatika', '2017', 'pagi;', 'rpl.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `daftar_dosen`
--
ALTER TABLE `daftar_dosen`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `daftar_jadwal`
--
ALTER TABLE `daftar_jadwal`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `daftar_mahasiswa`
--
ALTER TABLE `daftar_mahasiswa`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_form`
--
ALTER TABLE `user_form`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `user_form`
--
ALTER TABLE `user_form`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
