<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Document</title>
  <link rel="stylesheet" href="/edhuniv/media/css/admin/daftar_dosen.css" />
</head>

<body>
  <!-- NAVIGASI -->
  <nav>
    <div class="logo">
      <img src="/edhuniv/asset/icon/menu.png" class="menu-icon" />
      <span class="logo-name">Edh University</span>
    </div>
    <div class="container">
      <div class="logo">
        <img src="/edhuniv/asset/icon/menu.png" class="menu-icon" />
        <span class="logo-name">Edh University</span>
      </div>

      <div class="container-menu">
        <ul class="ul-navbar">
          <li class="li-navbar">
            <a href="/edhuniv/media/php/admin/mahasiswa/daftar_mahasiswa.php" class="navigasi">
              <img src="/edhuniv/asset/icon/daftar-mahasiswa.png" class="icon" />
              <span class="link">Daftar Mahasiswa</span>
            </a>
          </li>
          <li class="li-navbar">
            <a href="/edhuniv/media/php/admin/dosen/daftar_dosen.php" class="navigasi">
              <img src="/edhuniv/asset/icon/daftar-dosen.png" class="icon" />
              <span class="link">Daftar Dosen</span>
            </a>
          </li>
          <li class="li-navbar">
            <a href="/edhuniv/media/php/admin/jadwal/jadwal.php" class="navigasi">
              <img src="/edhuniv/asset/icon/jadwal.png" class="icon" />
              <span class="link">Jadwal</span>
            </a>
          </li>
        </ul>

        <div class="bottom-cotent">
          <ul class="ul-navbar">
            <li class="li-navbar">
              <a href="#" class="navigasi">
                <img src="/edhuniv/asset/icon/setting.png" class="icon" />
                <span class="link">Settings</span>
              </a>
            </li>
            <li class="li-navbar">
              <a href="/edhuniv/media/php/login.php" class="navigasi">
                <img src="/edhuniv/asset/icon/log-out.png" class="icon" />
                <span class="link">Logout</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>

  <!-- DAFTAR MAHASISWA -->
  <div class="table">
    <div class="table_header">
      <a href="/edhuniv/media/php/admin/dosen/daftar_dosen.php" type="button">Refresh</a>
      <p>Daftar Dosen University EDH Madiun</p>
      <?=(isset($_GET['pesan']) && !empty($_GET['pesan'])) ? "<i>" . $_GET['pesan'] . "</i>" : ""; ?>
    </div>
    <div class="table_section">
      <table class="tbl">
        <thead class="thead">
          <tr>
            <th>NO</th>
            <th>NIP/NIDN</th>
            <th>Nama</th>
            <th>Jenis Kelamin</th>
            <th>Program Studi</th>
            <th>Action</th>
          </tr>
        </thead>

        <?php
        include '../../koneksi.php';
        $no = 0;
        $ambildata = mysqli_query($koneksi, "SELECT * FROM daftar_dosen");
        while ($tampil = mysqli_fetch_array($ambildata)) {
          $no++;
        ?>
        <tbody>
          <tr>
            <td>
              <?php echo $no ?>
            </td>
            <td>
              <?php echo $tampil['Nip_Nidn']; ?>
            </td>
            <td>
              <?php echo $tampil['Nama'] ?>
            </td>
            <td>
              <?php echo $tampil['Jenis_kelamin']; ?>
            </td>
            <td>
              <?php echo $tampil['Program_studi']; ?>
            </td>
            <td>
              <a href="/edhuniv/media/php/admin/dosen/edit.php?Id=<?= $tampil['Id']; ?>"><button class="edit"
                  id="edite">
                  <img src="/edhuniv/asset/icon/edit.png" width="25px" />
                </button>
              </a>
              <a href="/edhuniv/media/php/admin/dosen/delete.php?Id=<?= $tampil['Id']; ?>"><button class="delete"
                  id="delete">
                  <img src="/edhuniv/asset/icon/delete.png" width="25px" />
              </a>
              </button>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    <div class="tambah">
      <p>
        tambah Data Dosen
        <button type="submit" id="button" class="add">add</button>
      </p>
    </div>
  </div>

  <!-- POPUP TAMBAH DATA -->
  <div class="container-popup">
    <div class="content-popup">
      <div class="exit">X</div>
      <div class="title">
        <h3>Tambah Data</h3>
      </div>
      <div class="contentpopup">
        <form method="post" action="/edhuniv/media/php/admin/dosen/create.php">
          <div class="user-detailspopup">
            <div class="input-box-popup">
              <span class="detailspopup">Id</span>
              <input type="text" name="Id" value="<?= rand(100, 999); ?>" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Nama</span>
              <input type="text" name="Nama" value="" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">NIP/NIDN</span>
              <input type="text" name="Nip_Nidn" value="" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Jenis_kelamin</span>
              <input type="text" name="Jenis_kelamin" value="" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Program Studi</span>
              <input type="text" name="Program_studi" value="" required />
            </div>
          </div>
          <div class="button">
            <a href="#container2">
              <input type="submit" value="Simpan" name="simpan" class="save" /></a>
          </div>
        </form>
      </div>
    </div>
  </div>

  <!-- EDIT DATA -->
  <!-- <div class="container-popup-edit">
    <div class="content-popup-edit">
      <div class="exit">X</div>
      <div class="title">
        <h3>Edit Data</h3>
      </div>
      <div class="contentpopup">
        <form action="#">
          <div class="user-detailspopup">
            <div class="input-box-popup">
              <span class="detailspopup">Nama</span>
              <input type="text" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">NIP/NIDN</span>
              <input type="text" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Program Studi</span>
              <input type="text" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Action</span>
              <input type="text" required />
            </div>
          </div>
          <div class="button">
            <a href="#container2">
              <input type="submit" value="Simpan" name="simpan" class="save" /></a>
          </div>
        </form>
      </div>
    </div>
  </div> -->

  <!-- HAPUS DATA -->
  <div class="container-popup-hapus">
    <div class="content-popup-hapus">
      <div class="exit">X</div>
      <div class="title">
        <h3>Tambah Data</h3>
      </div>
      <div class="contentpopup">
        <form action="#">
          <div class="user-detailspopup">
            <div class="input-box-popup">
              <span class="detailspopup">Nama</span>
              <input type="text" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">NIP/NIDN</span>
              <input type="text" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Program Studi</span>
              <input type="text" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Action</span>
              <input type="text" required />
            </div>
          </div>
          <div class="button">
            <a href="#container2">
              <input type="submit" value="Simpan" name="simpan" class="save" /></a>
          </div>
        </form>
      </div>
    </div>
  </div>

  <script>
    // TAMBAH DATA
    document.getElementById("button").addEventListener("click", function () {
      document.querySelector(".container-popup").style.display = "flex";
    });

    document.querySelector(".save").addEventListener("click", function () {
      document.querySelector(".container-popup").style.display = "none";
    });

    document.querySelector(".exit").addEventListener("click", function () {
      document.querySelector(".container-popup").style.display = "none";
    });

    // // TAMBAH DATA
    // cument.getElementById("button").addEventListener("click", function () {
    //   ment.querySelector(".container-popup").style.display = "flex";
    //   do    .querySelector(".save").addEventListener("click", function () {
    //     do uerySelector(".container-popup").style.display = "none";
    // });

    //  ument    Selector(".exit").addEventListener("click", function () {
    //   document      lector(".container-popup").style.display = "none";
    //     });

    //   //E    TA
    //   ment.getElem    d("edite").addEventListener("click", function () {
    //     document.querySe.container - popup").style.display = "flex";
    //   });

    //   document.qu    ector    e").addEventListener("click", function () {
    //   document.querySelector      iner - popup").style.display = "none";
    // });

    // document.querySel    ".exi    dEventListener("click", function () {
    // document.querySelector(".con      opup").style.display = "none";
    // });

    // // HAPUS DATA
    // docum    tElem    d("deletee").a    tListener("click", function () {
    //   document.querySelector(".container-p      tyle.display = "flex";
    // });

    // document.querySelector(".save")    entLi("click", function () {
    //   document.querySelector(".container-popup")      isplay = "none";
    // });

    // document.querySelector(".exit").addEv    tener    k", function () {
    // document.querySelector(".container-popup").style = "none";
    // });
  </script>
  <section class="overlay"></section>
  <script src="/edhuniv/media/js/main.js"></script>
</body>

</html>