<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/edhuniv/media/css/admin/daftar_mahasiswa.css" />
    <title>Edit Dosen</title>
</head>

<body>

<?php
    include '../../koneksi.php';

    $Id = $_GET['Id'];

    (!isset($Id) && empty($Id)) ? header('location: /edhuniv/media/php/admin/dosen/Daftar_dosen.php') : '';

    $query = "SELECT * FROM daftar_dosen WHERE Id = $Id LIMIT 1";

    $hasil_query = mysqli_query($koneksi, $query);

    $data = mysqli_fetch_assoc($hasil_query);

    empty($data) ? header('location: /edhuniv/media/php/admin/dosen/daftar_dosen.php') : '';

    ?>
    <div class="container-popup-edit">
        <div class="content-popup-edit">
            <a href="/edhuniv/media/php/admin/dosen/daftar_dosen.php">
                <div class="exit">X</div>
            </a>
            <div class="title">
                <h3>Edit Data</h3>
            </div>
            <div class="contentpopup">
                <form method="post" action="/edhuniv/media/php/admin/dosen/update.php">
                    <div class="user-detailspopup">
                        <div class="input-box-popup">
                            <span class="detailspopup">Id</span>
                            <input type="text" name="Id" value="<?= $data['Id']; ?>" required />
                        </div>
                        <div class="input-box-popup">
                            <span class="detailspopup">Nama</span>
                            <input type="text" name="Nama" value="<?= $data['Nama']; ?>" required />
                        </div>
                        <div class="input-box-popup">
                            <span class="detailspopup">NIP/NIDN</span>
                            <input type="text" name="Nip_Nidn" value="<?= $data['Nip_Nidn']; ?>" required />
                        </div>
                        <div class="input-box-popup">
                            <span class="detailspopup">Jenis Kelamin</span>
                            <input type="text" name="Jenis_kelamin" value="<?= $data['Jenis_kelamin']; ?>" required />
                        </div>
                        <div class="input-box-popup">
                            <span class="detailspopup">Program Studi</span>
                            <input type="text" name="Program_studi" value="<?= $data['Program_studi']; ?>" required />
                        </div>
                    </div>
                    <div class="button">
                        <a href="#container2">
                            <input type="submit" value="Simpan" name="simpan" class="save" /></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>

</html>