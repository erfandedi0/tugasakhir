<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/edhuniv/media/css/admin/daftar_mahasiswa.css" />
    <title>Edit Jadwal</title>
</head>

<body>
    
    <?php
    include '../../koneksi.php';

    $Id = $_GET['Id'];

    (!isset($Id) && empty($Id)) ? header('location: /edhuniv/media/php/admin/jadwal/update.php') : '';

    $query = "SELECT * FROM daftar_jadwal WHERE Id = $Id LIMIT 1";

    $hasil_query = mysqli_query($koneksi, $query);

    $data = mysqli_fetch_assoc($hasil_query);

    empty($data) ? header('location: /edhuniv/media/php/admin/jadwal/update.php') : '';

    ?>
    <div class="content-popup-edit">
        <a href="/edhuniv/media/php/admin/jadwal/jadwal.php">
            <div class="exit">X</div>
        </a>
        <div class="title">
            <h3>Edit Data</h3>
        </div>
        <div class="contentpopup">
            <form method="post" action="/edhuniv/media/php/admin/jadwal/update.php">
                <div class="user-detailspopup">
                    <div class="input-box-popup">
                        <span class="detailspopup">Id</span>
                        <input type="text" name="Id" value="<?= $data['Id']; ?>" required />
                    </div>
                    <div class="input-box-popup">
                        <span class="detailspopup">Kode Matkul</span>
                        <input type="text" name="Kode_matkul" value="<?= $data['Kode_matkul']; ?>" required />
                    </div>
                    <div class="input-box-popup">
                        <span class="detailspopup">Tanggal</span>
                        <input type="text" name="Tanggal" value="<?= $data['Tanggal']; ?>" required />
                    </div>
                    <div class="input-box-popup">
                        <span class="detailspopup">Waktu</span>
                        <input type="text" name="Waktu" value="<?= $data['Waktu']; ?>" required />
                    </div>
                    <div class="input-box-popup">
                        <span class="detailspopup">Matakuliah</span>
                        <input type="text" name="Matakuliah" value="<?= $data['Matakuliah']; ?>" required />
                    </div>
                    <div class="input-box-popup">
                        <span class="detailspopup">Prodi</span>
                        <input type="text" name="Prodi" value="<?= $data['Prodi']; ?>" required />
                    </div>
                    <div class="input-box-popup">
                        <span class="detailspopup">Semester</span>
                        <input type="text" name="Semester" value="<?= $data['Semester']; ?>" required />
                    </div>
                    <div class="input-box-popup">
                        <span class="detailspopup">Kelas</span>
                        <input type="text" name="Kelas" value="<?= $data['Kelas']; ?>" required />
                    </div>
                </div>
                <div class="button">
                    <a href="#container2">
                        <input type="submit" value="Simpan" name="simpan" class="save" /></a>
                </div>
            </form>
        </div>
    </div>
</body>

</html>