<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Document</title>
  <link rel="stylesheet" href="/edhuniv/media/css/admin/jadwal.css" />
</head>

<body>
  <!-- NAVIGASI -->
  <nav>
    <div class="logo">
      <img src="/edhuniv/asset/icon/menu.png" class="menu-icon" />
      <span class="logo-name">Edh University</span>
    </div>
    <div class="container">
      <div class="logo">
        <img src="/edhuniv/asset/icon/menu.png" class="menu-icon" />
        <span class="logo-name">Edh University</span>
      </div>

      <div class="container-menu">
        <ul class="ul-navbar">
          <li class="li-navbar">
            <a href="/edhuniv/media/php/admin/mahasiswa/daftar_mahasiswa.php" class="navigasi">
              <img src="/edhuniv/asset/icon/daftar-mahasiswa.png" class="icon" />
              <span class="link">Daftar Mahasiswa</span>
            </a>
          </li>
          <li class="li-navbar">
            <a href="/edhuniv/media/php/admin/dosen/daftar_dosen.php" class="navigasi">
              <img src="/edhuniv/asset/icon/daftar-dosen.png" class="icon" />
              <span class="link">Daftar Dosen</span>
            </a>
          </li>
          <li class="li-navbar">
            <a href="/edhuniv/media/php/admin/jadwal/jadwal.php" class="navigasi">
              <img src="/edhuniv/asset/icon/jadwal.png" class="icon" />
              <span class="link">Jadwal</span>
            </a>
          </li>
        </ul>

        <div class="bottom-cotent">
          <ul class="ul-navbar">
            <li class="li-navbar">
              <a href="#" class="navigasi">
                <img src="/edhuniv/asset/icon/setting.png" class="icon" />
                <span class="link">Settings</span>
              </a>
            </li>
            <li class="li-navbar">
              <a href="/edhuniv/media/php/login.php" class="navigasi">
                <img src="/edhuniv/asset/icon/log-out.png" class="icon" />
                <span class="link">Logout</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>

  <!-- DAFTAR MAHASISWA -->
  <div class="table">
    <div class="table_header">
      <a href="/edhuniv/media/php/admin/jadwal/jadwal.php" type="button">Refresh</a>
      <p>Jadwal University EDH Madiun</p>
      <?=(isset($_GET['pesan']) && !empty($_GET['pesan'])) ? "<i>" . $_GET['pesan'] . "</i>" : ""; ?>
    </div>
    <div class="table_section">
      <table class="tbl">
        <thead class="thead">
          <tr>
            <th>No</th>
            <th>Kode Matkul</th>
            <th>Hari/Tanggal</th>
            <th>Waktu</th>
            <th>Matakuliah</th>
            <th>Prodi</th>
            <th>Semester</th>
            <th>Kelas</th>
            <th>Action</th>
          </tr>
        </thead>

        <?php
        include '../../koneksi.php';
        $no = 0;
        $ambildata = mysqli_query($koneksi, "SELECT * FROM daftar_jadwal");
        while ($tampil = mysqli_fetch_array($ambildata)) {
          $no++;
        ?>
        <tbody>
          <tr>
            <td>
              <?php echo $no ?>
            </td>
            <td>
              <?php echo $tampil['Kode_matkul']; ?>
            </td>
            <td>
              <?php echo $tampil['Tanggal'] ?>
            </td>
            <td>
              <?php echo $tampil['Waktu']; ?>
            </td>
            <td>
              <?php echo $tampil['Matakuliah']; ?>
            </td>
            <td>
              <?php echo $tampil['Prodi']; ?>
            </td>
            <td>
              <?php echo $tampil['Semester']; ?>
            </td>
            <td>
              <?php echo $tampil['Kelas']; ?>
            </td>
            <td>
              <a href="/edhuniv/media/php/admin/jadwal/edit.php?Id=<?= $tampil['Id']; ?>"><button class="edit"
                  id="edite">
                  <img src="/edhuniv/asset/icon/edit.png" width="25px" />
                </button>
              </a>
              <a href="/edhuniv/media/php/admin/jadwal/delete.php?Id=<?= $tampil['Id']; ?>"><button class="delete"
                  id="delete">
                  <img src="/edhuniv/asset/icon/delete.png" width="25px" />
              </a>
              </button>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    <div class="tambah">
      <p>
        tambah Data Penjadwalan
        <button type="submit" class="add" id="button">add</button>
      </p>
    </div>
  </div>

  <!-- POPUP TAMBAH DATA -->
  <div class="container-popup">
    <div class="content-popup">
      <div class="exit">X</div>
      <div class="title">
        <h3>Tambah Data</h3>
      </div>
      <div class="contentpopup">
        <form method="post" action="/edhuniv/media/php/admin/jadwal/create.php">
          <div class="user-detailspopup">
            <div class="input-box-popup">
              <span class="detailspopup">Id</span>
              <input type="text" name="Id" value="<?= rand(100, 999); ?>" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Kode Matkul</span>
              <input type="text" name="Kode_matkul" value="" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Tanggal</span>
              <input type="text" name="Tanggal" value="" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Waktu</span>
              <input type="text" name="Waktu" value="" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Matakuliah</span>
              <input type="text" name="Matakuliah" value="" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Prodi</span>
              <input type="text" name="Prodi" value="" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Semester</span>
              <input type="text" name="Semester" value="" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Kelas</span>
              <input type="text" name="Kelas" required />
            </div>
          </div>
          <div class="button">
            <a href="#container2">
              <input type="submit" value="Simpan" name="simpan" class="save" /></a>
          </div>
        </form>
      </div>
    </div>
  </div>

  <!-- POPUP EDIT -->
  <!-- <div class="container-popup-edit">
    <div class="content-popup-edit">
      <div class="exit">X</div>
      <div class="title">
        <h3>Edit Data</h3>
      </div>
      <div class="contentpopup">
        <form action="#">
          <div class="user-detailspopup">
            <div class="input-box-popup">
              <span class="detailspopup">Kode Matkul</span>
              <input type="text" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Tanggal</span>
              <input type="text" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Waktu</span>
              <input type="text" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Matakuliah</span>
              <input type="text" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Prodi</span>
              <input type="text" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Semester</span>
              <input type="text" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Kelas</span>
              <input type="text" required />
            </div>
          </div>
          <div class="button">
            <a href="#container2">
              <input type="submit" value="Simpan" name="simpan" class="save" /></a>
          </div>
        </form>
      </div>
    </div>
  </div> -->

  <!-- POPUP HAPUS -->
  <div class="container-popup-hapus">
    <div class="content-popup-hapus">
      <div class="exit">X</div>
      <div class="title">
        <h3>Hapus Data</h3>
      </div>
      <div class="contentpopup">
        <form action="#">
          <div class="user-detailspopup">
            <div class="input-box-popup">
              <span class="detailspopup">Kode Matkul</span>
              <input type="text" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Tanggal</span>
              <input type="text" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Waktu</span>
              <input type="text" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Matakuliah</span>
              <input type="text" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Prodi</span>
              <input type="text" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Semester</span>
              <input type="text" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Kelas</span>
              <input type="text" required />
            </div>
          </div>
          <div class="button">
            <a href="#container2">
              <input type="submit" value="Simpan" name="simpan" class="save" /></a>
          </div>
        </form>
      </div>
    </div>
  </div>

  <script>
    // TAMBAH DATA
    document.getElementById("button").addEventListener("click", function () {
      document.querySelector(".container-popup").style.display = "flex";
    });

    document.querySelector(".save").addEventListener("click", function () {
      document.querySelector(".container-popup").style.display = "none";
    });

    document.querySelector(".exit").addEventListener("click", function () {
      document.querySelector(".container-popup").style.display = "none";
    });

    // EDIT DATA
    // document.getElementById("edite").addEventListener("click", function () {
    //   document.querySelector(".container-popup").style.display = "flex";
    // });

    // document.querySelector(".save").addEventListener("click", function () {
    //   document.querySelector(".container-popup").style.display = "none";
    // });

    // document.querySelector(".exit").addEventListener("click", function () {
    //   document.querySelector(".container-popup").style.display = "none";
    // });

    // HAPUS DATA
    // document.getElementById("deletee").addEventListener("click", function () {
    //   document.querySelector(".container-popup").style.display = "flex";
    // });

    // document.querySelector(".save").addEventListener("click", function () {
    //   document.querySelector(".container-popup").style.display = "none";
    // });

    // document.querySelector(".exit").addEventListener("click", function () {
    //   document.querySelector(".container-popup").style.display = "none";
    // });
  </script>

  <section class="overlay"></section>
  <script src="/edhuniv/media/js/main.js"></script>
</body>

</html>