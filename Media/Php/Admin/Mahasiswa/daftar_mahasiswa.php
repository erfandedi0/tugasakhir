<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Daftar Mahasiswa</title>
  <link rel="stylesheet" href="/edhuniv/media/css/admin/daftar_mahasiswa.css" />
</head>

<body>
  <!-- NAVIGASI -->
  <nav>
    <div class="logo">
      <img src="/edhuniv/asset/icon/menu.png" class="menu-icon" />
      <span class="logo-name">Edh University</span>
    </div>
    <div class="container">
      <div class="logo">
        <img src="/edhuniv/asset/icon/menu.png" class="menu-icon" />
        <span class="logo-name">Edh University</span>
      </div>

      <div class="container-menu">
        <ul class="ul-navbar">
          <li class="li-navbar">
            <a href="/edhuniv/media/php/admin/mahasiswa/daftar_mahasiswa.php" class="navigasi">
              <img src="/edhuniv/asset/icon/daftar-mahasiswa.png" class="icon" />
              <span class="link">Daftar Mahasiswa</span>
            </a>
          </li>
          <li class="li-navbar">
            <a href="/edhuniv/media/php/admin/dosen/daftar_dosen.php" class="navigasi">
              <img src="/edhuniv/asset/icon/daftar-dosen.png" class="icon" />
              <span class="link">Daftar Dosen</span>
            </a>
          </li>
          <li class="li-navbar">
            <a href="/edhuniv/media/php/admin/jadwal/jadwal.php" class="navigasi">
              <img src="/edhuniv/asset/icon/jadwal.png" class="icon" />
              <span class="link">Jadwal</span>
            </a>
          </li>
        </ul>

        <div class="bottom-cotent">
          <ul class="ul-navbar">
            <li class="li-navbar">
              <a href="#" class="navigasi">
                <img src="/edhuniv/asset/icon/setting.png" class="icon" />
                <span class="link">Settings</span>
              </a>
            </li>
            <li class="li-navbar">
              <a href="/edhuniv/media/php/login.php" class="navigasi">
                <img src="/edhuniv/asset/icon/log-out.png" class="icon" />
                <span class="link">Logout</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>

  <!-- DAFTAR MAHASISWA -->
  <!-- TAMPILAN DATA -->
  <div class="table">
    <div class="table_header">
      <a href="/edhuniv/media/php/admin/mahasiswa/daftar_mahasiswa.php" type="button">Refresh</a>
      <p>Daftar Mahasiswa University EDH Madiun</p>
      <?=(isset($_GET['pesan']) && !empty($_GET['pesan'])) ? "<i>" . $_GET['pesan'] . "</i>" : ""; ?>
    </div>
    <div class="table_section">
      <table class="tbl">
        <thead class="thead">
          <tr>
            <th>No</th>
            <th>NIM</th>
            <th>Nama</th>
            <th>Jenis Kelamin</th>
            <th>Prodi</th>
            <th>Semester</th>
            <th>Kelas</th>
            <th>Action</th>
          </tr>
        </thead>

        <?php
        include '../../koneksi.php';
        $no = 0;
        $ambildata = mysqli_query($koneksi, "SELECT * FROM daftar_mahasiswa");
        while ($tampil = mysqli_fetch_array($ambildata)) {
          $no++;
        ?>
        <tbody>
          <tr>
            <td>
              <?php echo $no ?>
            </td>
            <td>
              <?php echo $tampil['Nim']; ?>
            </td>
            <td>
              <?php echo $tampil['Nama'] ?>
            </td>
            <td>
              <?php echo $tampil['Jenis_kelamin']; ?>
            </td>
            <td>
              <?php echo $tampil['Prodi']; ?>
            </td>
            <td>
              <?php echo $tampil['Semester']; ?>
            </td>
            <td>
              <?php echo $tampil['Kelas']; ?>
            </td>
            <td>
              <a href="/edhuniv/media/php/admin/mahasiswa/edit.php?Id=<?= $tampil['Id']; ?>"><button class="edit"
                  id="edite" onclick="openPopup()">
                  <img src="/edhuniv/asset/icon/edit.png" width="25px" />
                </button>
              </a>
              <a href="/edhuniv/media/php/admin/mahasiswa/delete.php?Id=<?= $tampil['Id']; ?>"><button class="delete"
                  id="delete">
                  <img src="/edhuniv/asset/icon/delete.png" width="25px" />
              </a>
              </button>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
  <div class="tambah">
    <p>
      tambah Data Mahasiswa
      <button type="submit" id="button" class="add">add</button>
    </p>
  </div>

  <!-- POPUP TAMBAH DATA -->
  <div class="container-popup">
    <div class="content-popup">
      <div class="exit">X</div>
      <div class="title">
        <h3>Tambah Data</h3>
      </div>
      <div class="contentpopup">
        <form method="post" action="/edhuniv/media/php/admin/mahasiswa/create.php">
          <div class="user-detailspopup">
            <div class="input-box-popup">
              <span class="detailspopup">Id</span>
              <input type="text" name="Id" value="<?= rand(100, 999); ?>" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">NIM</span>
              <input type="text" name="Nim" value="" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Nama</span>
              <input type="text" name="Nama" value="" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Jenis Kelamin</span>
              <input type="text" name="Jenis_kelamin" value="" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Prodi</span>
              <input type="text" name="Prodi" value="" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Semester</span>
              <input type="text" name="Semester" value="" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Kelas</span>
              <input type="text" name="Kelas" value="" required />
            </div>
          </div>
          <div class="button">
            <a href="#container2">
              <input type="submit" value="Simpan" name="simpan" class="save" /></a>
          </div>
        </form>
      </div>
    </div>
  </div>


  <!-- POPUP EDIT -->
  <?php
//  include '../../koneksi.php';

//  $Id = $_GET['Id'];
//   $data = [];
//  (!isset($Id) && empty($Id)) ? header('location: /edhuniv/media/php/admin/daftar_mahasiswa.php') : '';
// if(isset($Id)){
//   $query = "SELECT * FROM daftar_mahasiswa WHERE Id = $Id LIMIT 1";
 
//   $hasil_query = mysqli_query($koneksi, $query);
  
//   $data = mysqli_fetch_assoc($hasil_query);
// }
 

 
//  empty($data) ? header('location: /edhuniv/media/php/admin/daftar_mahasiswa.php') : '';
 
 ?>

  <!-- <div class="popup" id="send">
    <div class="content-popup-edit">
      <div class="exit" onclick="closePopup()">X</div>
      <div class="title">
        <h3>Edit Data</h3>
      </div>
      <div class="contentpopup">
        <form method="post" action="/edhuniv/media/php/admin/mahasiswa/update.php">
          <div class="user-detailspopup">
            <div class="input-box-popup">
              <span class="detailspopup">Id</span>
              <input type="text" name="Id" value="<?= $data['Id']; ?>" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">NIM</span>
              <input type="text" name="Nim" value="<?= $data['Nim']; ?>" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Nama</span>
              <input type="text" name="Nama" value="<?= $data['Nama']; ?>" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Jenis Kelamin</span>
              <input type="text" name="Jenis_kelamin" value="<?= $data['Jenis_kelamin']; ?>" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Prodi</span>
              <input type="text" name="Prodi" value="<?= $data['Prodi']; ?>" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Semester</span>
              <input type="text" name="Semester" value="<?= $data['Semester']; ?>" required />
            </div>
            <div class="input-box-popup">
              <span class="detailspopup">Kelas</span>
              <input type="text" name="Kelas" value="<?= $data['Kelas']; ?>" required />
            </div>
          </div>
          <div class="button">
            <a href="#container2">
              <input type="submit" value="Simpan" name="simpan" class="save" /></a>
          </div>
        </form>
      </div>
    </div>
  </div>

  <script>
    let popup = document.getElementById("send");

    function openPopup() {
      popup.classList.add("open-popup");
    }
    function closePopup() {
      popup.classList.remove("open-popup");
    }
  </script> -->

  <!-- POPUP HAPUS -->
  <div class="popuphapus" id="hapus">
    <div class="content-popup">
      <div class="exit" onclick="closePopup()">X</div>
      <div class="title">
        <h3>Hapus Data</h3>
      </div>
      <div>Apakah Anda Yakin Untuk Menghapus</div>
      <div class="contentpopup">
        <form action="#">
          <div class="user-detailspopup">
          </div>
          <div class="button">
            <a href="#container2">
              <input type="submit" value="Simpan" name="simpan" class="save" /></a>
          </div>
        </form>
      </div>
    </div>
  </div>

  <script>
    let popup-delete = document.getElementById("hapus");

    function openPopup() {
      popup - delete.classList.add("open-popuphapus");
    }
    function closePopup() {
      popup - delete.classList.remove("open-popuphapus");
    }
  </script>

  <script>
    // TAMBAH DATA
    document.getElementById("button").addEventListener("click", function () {
      document.querySelector(".container-popup").style.display = "flex";
    });

    document.querySelector(".save").addEventListener("click", function () {
      document.querySelector(".container-popup").style.display = "none";
    });

    document.querySelector(".exit").addEventListener("click", function () {
      document.querySelector(".container-popup").style.display = "none";
    });

  </script>
  <section class="overlay"></section>
  <script src="/edhuniv/media/js/main.js"></script>
</body>

</html>