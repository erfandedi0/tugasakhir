<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Edit Mahasiswa</title>
  <link rel="stylesheet" href="/edhuniv/media/css/admin/daftar_mahasiswa.css" />
</head>

<body>

  <?php
  include '../../koneksi.php';

  $Id = $_GET['Id'];

  (!isset($Id) && empty($Id)) ? header('location: /edhuniv/media/php/admin/mahasiswa/daftar_mahasiswa.php') : '';

  $query = "SELECT * FROM daftar_mahasiswa WHERE Id = $Id LIMIT 1";

  $hasil_query = mysqli_query($koneksi, $query);

  $data = mysqli_fetch_assoc($hasil_query);

  empty($data) ? header('location: /edhuniv/media/php/admin/mahasiswa/daftar_mahasiswa.php') : '';

  ?>


  <div class="content-popup-edit">
    <a href="/edhuniv/media/php/admin/mahasiswa/daftar_mahasiswa.php">
      <div class="exit">X</div>
    </a>
    <div class="title">
      <h3>Edit Data</h3>
    </div>
    <div class="contentpopup">
      <form method="post" action="/edhuniv/media/php/admin/mahasiswa/update.php">
        <div class="user-detailspopup">
          <div class="input-box-popup">
            <span class="detailspopup">Id</span>
            <input type="text" name="Id" value="<?= $data['Id']; ?>" required />
          </div>
          <div class="input-box-popup">
            <span class="detailspopup">NIM</span>
            <input type="text" name="Nim" value="<?= $data['Nim']; ?>" required />
          </div>
          <div class="input-box-popup">
            <span class="detailspopup">Nama</span>
            <input type="text" name="Nama" value="<?= $data['Nama']; ?>" required />
          </div>
          <div class="input-box-popup">
            <span class="detailspopup">Jenis Kelamin</span>
            <input type="text" name="Jenis_kelamin" value="<?= $data['Jenis_kelamin']; ?>" required />
          </div>
          <div class="input-box-popup">
            <span class="detailspopup">Prodi</span>
            <input type="text" name="Prodi" value="<?= $data['Prodi']; ?>" required />
          </div>
          <div class="input-box-popup">
            <span class="detailspopup">Semester</span>
            <input type="text" name="Semester" value="<?= $data['Semester']; ?>" required />
          </div>
          <div class="input-box-popup">
            <span class="detailspopup">Kelas</span>
            <input type="text" name="Kelas" value="<?= $data['Kelas']; ?>" required />
          </div>
        </div>
        <div class="button">
          <a href="#container2">
            <input type="submit" value="Simpan" name="simpan" class="save" /></a>
        </div>
      </form>
    </div>
  </div>

  <!-- 
  <script>
    let popup = document.getElementById("send");

    function openPopup() {
      popup.classList.add("open-popup");
    }
    function closePopup() {
      popup.classList.remove("open-popup");
    }
  </script> -->

</body>

</html>