<?php

include 'koneksi.php';

if (isset($_POST['submit'])) {


	$name = mysqli_real_escape_string($koneksi, $_POST['name']);
	$email = mysqli_real_escape_string($koneksi, $_POST['email']);
	$nim = mysqli_real_escape_string($koneksi, $_POST['nim']);
	$nama_mahasiswa = mysqli_real_escape_string($koneksi, $_POST['nama_mahasiswa']);
	$tempat_tgl_lahir = mysqli_real_escape_string($koneksi, $_POST['tempat_tgl_lahir']);
	$jenis_kelamin = mysqli_real_escape_string($koneksi, $_POST['jenis_kelamin']);
	$jurusan = mysqli_real_escape_string($koneksi, $_POST['jurusan']);
	$kurikulum = mysqli_real_escape_string($koneksi, $_POST['kurikulum']);
	$sistem_kuliah = mysqli_real_escape_string($koneksi, $_POST['sistem_kuliah']);
	$pass = md5($_POST['password']);
	$cpass = md5($_POST['cpassword']);
	$image = $_FILES['image']['name'];
	$image_size = $_FILES['image']['size'];
	$image_tmp_name = $_FILES['image']['tmp_name'];
	$image_folder = '../../uploaded_img/' . $image;
	// $user_type = $_POST['user_type'];


	$select = " SELECT * FROM user_form WHERE email = '$email' && password = '$pass' ";

	$result = mysqli_query($koneksi, $select);

	if (mysqli_num_rows($result) > 0) {

		$error[] = 'user already exist!';

	} else {

		if ($pass != $cpass) {
			$error[] = 'password not matched!';
		} else {
			$insert = "INSERT INTO user_form (name, email, nim, nama_mahasiswa, tempat_tgl_lahir, jenis_kelamin, jurusan, kurikulum, sistem_kuliah, password, image ) VALUES ('$name','$email','$nim','$nama_mahasiswa','$tempat_tgl_lahir','$jenis_kelamin','$jurusan','$kurikulum','$sistem_kuliah','$pass','$image')";

			if ($insert) {
				move_uploaded_file($image_tmp_name, $image_folder);

				mysqli_query($koneksi, $insert);
				header('location:login.php');
			}
		}
	}
}
;


?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" type="text/css" href="../css/login.css">

	<title>Register Form </title>
</head>

<body>
	<div class="container">
		<form action="" method="POST" class="login-email" enctype="multipart/form-data">
			<p class="login-text" style="font-size: 2rem; font-weight: 800;">Register</p>
			<?php
			if (isset($error)) {
				foreach ($error as $error) {
					echo '<span class="error-msg">' . $error . '</span>';
				}
				;
			}
			;
			?>
			<div class="input-group">
				<input type="text" name="name" required placeholder="enter your name">
			</div>
			<div class="input-group">
				<input type="email" name="email" required placeholder="enter your email">
			</div>
			<div class="input-group">
				<input type="password" name="password" required placeholder="enter your password" minlength="8"
					maxlength="12">
			</div>
			<div class="input-group">
				<input type="password" name="cpassword" required placeholder="confirm your password">
			</div>
			<div>
				<p>Data Diri Mahasiswa</p>
				</br>
			</div>
			<div class="input-group">
				<input type="text" name="nim" required placeholder="enter your nim">
			</div>
			<div class="input-group">
				<input type="text" name="nama_mahasiswa" required placeholder="enter your nama_mahasiswa">
			</div>
			<div class="input-group">
				<input type="text" name="tempat_tgl_lahir" required placeholder="enter your tempat_tgl_lahir">
			</div>
			<div class="input-group">
				<input type="text" name="jenis_kelamin" required placeholder="enter your jenis_kelamin">
			</div>
			<div class="input-group">
				<input type="text" name="jurusan" required placeholder="enter your jurusan">
			</div>
			<div class="input-group">
				<input type="text" name="kurikulum" required placeholder="enter your tahun_kurikulum">
			</div>
			<div class="input-group">
				<input type="text" name="sistem_kuliah" required placeholder="enter your sistem_kuliah">
			</div>
			<div class="input-group">
				<input type="file" name="image" class="box" accept="image/jpg, image/jpeg, image/png">
			</div>
			<!-- <div class="input-group">
				<select name="user_type">
					<option value="user">user</option>
					<option value="admin">admin</option>
				</select>
			</div> -->
			<div class="input-group">
				<button name="submit" class="btn">Register</button>
			</div>
			<p class="login-register-text">Have an account? <a href="login.php">Login Here</a>.</p>
		</form>
	</div>
</body>

</html>