<?php

include 'koneksi.php';
session_start();
?>

<?php

@include 'koneksi.php';

if (isset($_POST['submit'])) {

	$name = mysqli_real_escape_string($koneksi, $_POST['name']);
	$email = mysqli_real_escape_string($koneksi, $_POST['email']);
	$pass = md5($_POST['password']);
	$cpass = md5($_POST['cpassword']);
	$user_type = $_POST['user_type'];

	$select = " SELECT * FROM user_form WHERE email = '$email' && password = '$pass' ";

	$result = mysqli_query($koneksi, $select);

	if (mysqli_num_rows($result) > 0) {

		$row = mysqli_fetch_array($result);

		if ($row['user_type'] == 'admin') {

			$_SESSION['admin_name'] = $row['id'];
			header('location:/edhuniv/media/php/admin/dosen/daftar_dosen.php');

		} elseif ($row['user_type'] == 'user') {

			$_SESSION['user_name'] = $row['id'];
			header('location:/edhuniv/media/php/user/profil.php');

		}
		// elseif ($_SESSION['log'] == 'Logged') {
		// header('location:/edhuniv/media/php/admin/dosen/daftar_dosen.php');
		// }

	} else {
		echo 'Username atau password salah';
		header("location:/edhuniv/media/php/login.php");
	}

}
;
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="../css/login.css">
	<title>Login EdhUniv</title>
	<script type="text/javascript">history.pushState(null, null, location.href = "/edhuniv/media/php/login.php");
		window.onpopstate = function () {
			history.go(1);
		};
	</script>

	<title>LOGIN</title>
</head>

<body>
	<div class="container">
		<form action="" method="POST" class="login-email">
			<p class="login-text" style="font-size: 2rem; font-weight: 800;">Login</p>
			<div class="input-group">
				<input type="email" placeholder="Email" name="email" required>
			</div>
			<div class="input-group">
				<input type="password" placeholder="Password" name="password" required>
			</div>
			<div class="input-group">
				<button name="submit" class="btn">Login</button>
			</div>
			<p class="login-register-text">Don't have an account? <a href="register.php">Register Here</a>.</p>
		</form>
	</div>
</body>

</html>