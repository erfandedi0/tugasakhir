<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="/edhuniv/media/css/user/daftar-mahasiswa.css" />
</head>

<body>
    <!-- NAVIGASI -->
    <nav>
        <div class="logo">
            <img src="/edhuniv/asset/icon/menu.png" class="menu-icon" />
            <span class="logo-name">Edh University</span>
        </div>
        <div class="container">
            <div class="logo">
                <img src="/edhuniv/asset/icon/menu.png" class="menu-icon" />
                <span class="logo-name">Edh University</span>
            </div>

            <div class="container-menu">
                <ul class="ul-navbar">
                    <li class="li-navbar">
                        <a href="/edhuniv/media/php/user/profil.php" class="navigasi">
                            <img src="/edhuniv/asset/icon/profil.png" class="icon" />
                            <span class="link">Profil</span>
                        </a>
                    </li>
                    <li class="li-navbar">
                        <a href="/edhuniv/media/php/user/dashboard.php" class="navigasi">
                            <img src="/edhuniv/asset/icon/home.png" class="icon" />
                            <span class="link">Dashboard</span>
                        </a>
                    </li>
                    <li class="li-navbar">
                        <a href="/edhuniv/media/php/user/daftar-mahasiswa.php" class="navigasi">
                            <img src="/edhuniv/asset/icon/daftar-mahasiswa.png" class="icon" />
                            <span class="link">Daftar Mahasiswa</span>
                        </a>
                    </li>
                    <li class="li-navbar">
                        <a href="/edhuniv/media/php/user/daftar-dosen.php" class="navigasi">
                            <img src="/edhuniv/asset/icon/daftar-dosen.png" class="icon" />
                            <span class="link">Daftar Dosen</span>
                        </a>
                    </li>
                    <li class="li-navbar">
                        <a href="/edhuniv/media/php/user/jadwal.php" class="navigasi">
                            <img src="/edhuniv/asset/icon/jadwal.png" class="icon" />
                            <span class="link">Jadwal</span>
                        </a>
                    </li>
                </ul>

                <div class="bottom-cotent">
                    <ul class="ul-navbar">
                        <li class="li-navbar">
                            <a href="#" class="navigasi">
                                <img src="/edhuniv/asset/icon/setting.png" class="icon" />
                                <span class="link">Settings</span>
                            </a>
                        </li>
                        <li class="li-navbar">
                            <a href="/edhuniv/media/php/login.php" class="navigasi">
                                <img src="/edhuniv/asset/icon/log-out.png" class="icon" />
                                <span class="link">Logout</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    <!-- DAFTAR MAHASISWA -->
    <div class="table">
        <div class="table_header">
            <p>Daftar Mahasiswa University EDH Madiun</p>
        </div>
        <div class="table_section">
            <table class="tbl">
                <thead class="thead">
                    <tr>
                        <th>No</th>
                        <th>NIM</th>
                        <th>Nama</th>
                        <th>Jenis Kelamin</th>
                        <th>Prodi</th>
                        <th>Semester</th>
                        <th>Kelas</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    include('../koneksi.php');
                    $no=1;
                    $ambildata = mysqli_query($koneksi, "SELECT * FROM daftar_mahasiswa");
                    WHILE ($tampil = mysqli_fetch_array($ambildata)){
                        echo
                    "<tr>
                        <td>$no</td>
                        <td>$tampil[Nim]</td>
                        <td>$tampil[Nama]</td>
                        <td>$tampil[Jenis_kelamin]</td>
                        <td>$tampil[Prodi]</td>
                        <td>$tampil[Semester]</td>
                        <td>$tampil[Kelas]</td>
                    </tr> ";
                    $no++;
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="caption">Tahun Ajaran 2022/2023</div>
    </div>


    <section class="overlay"></section>
    <script src="/edhuniv/media/js/main.js"></script>


</body>

</html>