<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/edhuniv/media/css/user/data-diri.css" />
    <title>Document</title>
</head>

<body>
    <div class="wrapper">
        <div class="text">
            SILAKAN ISI DATA DIRI ANDA
        </div>
        <div class="box">
            <div class="inputtext">
                <label>NIM</label>
                <input type="text" class="input">
            </div>
            <div class="inputtext">
                <label>Nama Mahasiswa</label>
                <input type="text" class="input">
            </div>
            <div class="inputtext">
                <label>Tempat,Tanggal Lahir</label>
                <input type="text" class="input">
            </div>
            <div class="inputtext">
                <label>Jenis Kelamin</label>
                <input type="text" class="input">
            </div>
            <div class="inputtext">
                <label>Alamat Email</label>
                <input type="text" class="input">
            </div>
            <div class="inputtext">
                <label>Jurusan</label>
                <input type="text" class="input">
            </div>
            <div class="inputtext">
                <label>Tahun Kurikulum</label>
                <input type="text" class="input">
            </div>
            <div class="inputtext">
                <label>Sistem Kuliah</label>
                <input type="text" class="input">
            </div>
            <div class="inputtext">
                <label>Silakan Kirim Foto Profil</label>
                <input type="file" class="send">
            </div>
            <div class="inputtext">
                <input type="submit" value="Kirim" class="btn" onclick="openPopup()">
            </div>
        </div>
    </div>

    <!-- POPUP DATA PRIBADI -->
        <div class="popup" id="send">
            <img src="/edhuniv/asset/icon/ceklis.png">
            <h2>Successful</h2>
            <p>Data diri anda telah berhasil dikirim.Silakan simpan bila data benar,jika salah coba isi kembali</p>
            <a href="/edhuniv/media/php/user/profil.php"><button class="oke">Simpan</button></a>
            <button type="submit" class="back" onclick="closePopup()">Kembali</button>
        </div>

        <script>
            let popup = document.getElementById("send");

            function openPopup() {
                popup.classList.add("open-popup");
            }
            function closePopup() {
                popup.classList.remove("open-popup");
            }
        </script>
</body>

</html>