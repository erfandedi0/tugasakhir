<?php

include '../koneksi.php';
session_start();
$user_name = $_SESSION['user_name']

    // if(!isset($user_id)){
//    header('location:login.php');
// };

    // if(isset($_GET['logout'])){
//    unset($user_id);
//    session_destroy();
//    header('location:login.php');
// }

    ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="/edhuniv/media/css/user/profil.css" />
</head>

<body>
    <!-- NAVIGASI -->
    <nav>
        <div class="logo">
            <img src="/edhuniv/asset/icon/menu.png" class="menu-icon" />
            <span class="logo-name">Edh University</span>
        </div>
        <div class="container">
            <div class="logo">
                <img src="/edhuniv/asset/icon/menu.png" class="menu-icon" />
                <span class="logo-name">Edh University</span>
            </div>

            <div class="container-menu">
                <ul class="ul-navbar">
                    <li class="li-navbar">
                        <a href="/edhuniv/media/php/user/profil.php" class="navigasi">
                            <img src="/edhuniv/asset/icon/profil.png" class="icon" />
                            <span class="link">Profil</span>
                        </a>
                    </li>
                    <li class="li-navbar">
                        <a href="/edhuniv/media/php/user/dashboard.php" class="navigasi">
                            <img src="/edhuniv/asset/icon/home.png" class="icon" />
                            <span class="link">Dashboard</span>
                        </a>
                    </li>
                    <li class="li-navbar">
                        <a href="/edhuniv/media/php/user/daftar-mahasiswa.php" class="navigasi">
                            <img src="/edhuniv/asset/icon/daftar-mahasiswa.png" class="icon" />
                            <span class="link">Daftar Mahasiswa</span>
                        </a>
                    </li>
                    <li class="li-navbar">
                        <a href="/edhuniv/media/php/user/daftar-dosen.php" class="navigasi">
                            <img src="/edhuniv/asset/icon/daftar-dosen.png" class="icon" />
                            <span class="link">Daftar Dosen</span>
                        </a>
                    </li>
                    <li class="li-navbar">
                        <a href="/edhuniv/media/php/user/jadwal.php" class="navigasi">
                            <img src="/edhuniv/asset/icon/jadwal.png" class="icon" />
                            <span class="link">Jadwal</span>
                        </a>
                    </li>
                </ul>

                <div class="bottom-cotent">
                    <ul class="ul-navbar">
                        <li class="li-navbar">
                            <a href="#" class="navigasi">
                                <img src="/edhuniv/asset/icon/setting.png" class="icon" />
                                <span class="link">Settings</span>
                            </a>
                        </li>
                        <li class="li-navbar">
                            <a href="/edhuniv/media/php/login.php" class="navigasi">
                                <img src="/edhuniv/asset/icon/log-out.png" class="icon" />
                                <span class="link">Logout</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    <!-- PROFIL -->
    <div class="container2">
        <div class="title">Profil</div>
        <div class="img">
            <!-- <img src="/asset/jpg/analisa-desain-sistem.jpg" width="200px" height="200px"> -->
            <?php
            include "../koneksi.php";
            $select = mysqli_query($koneksi, "SELECT * FROM `user_form` WHERE id = '$user_name'") or die('query failed');
            if (mysqli_num_rows($select) > 0) {
                $fetch = mysqli_fetch_assoc($select);
            }
            if ($fetch['image'] == '') {
                echo '<img src="images/default-avatar.png">';
            } else {
                echo '<img src="/edhuniv/uploaded_img/' . $fetch['image'] . '">';
            }
            ?>
        </div>
        <div class="content">
            <form action="#">
                <div class="user-details">
                    <div class="input-box">
                        <span class="details">NIM</span>
                        <div class="date"> <?php echo $fetch['nim']; ?></div>
                    </div>
                    <div class="input-box">
                        <span class="details">Nama Mahasiswa</span>
                        <div class="date">
                            <?php echo $fetch['nama_mahasiswa']; ?>
                        </div>
                    </div>
                    <div class="input-box">
                        <span class="details">Tempat,Tanggal Lahir</span>
                        <div class="date">
                            <?php echo $fetch['tempat_tgl_lahir']; ?>
                        </div>
                    </div>
                    <div class="input-box">
                        <span class="details">Jenis Kelamin</span>
                        <div class="date"> <?php echo $fetch['jenis_kelamin']; ?></div>
                    </div>
                    <div class="input-box">
                        <span class="details">Alamat Email</span>
                        <div class="date">
                            <?php echo $fetch['email']; ?>
                        </div>
                    </div>
                    <div class="input-box">
                        <span class="details">Jurusan</span>
                        <div class="date"> <?php echo $fetch['jurusan']; ?></div>
                    </div>
                    <div class="input-box">
                        <span class="details">Tahun Ajaran</span>
                        <div class="date">
                            <?php echo $fetch['kurikulum']; ?>
                        </div>
                    </div>
                    <div class="input-box">
                        <span class="details">Sistem Kuliah</span>
                        <div class="date"> <?php echo $fetch['sistem_kuliah']; ?></div>
                    </div>
                </div>
                <div class="button">
                    <input type="submit" value="Update" id="button" class="input">
                </div>
            </form>
        </div>
    </div>


    <!--POPUP UPDATE PROFIL -->
    <?php
    if (isset($_POST['submit'])) {

        $nim = mysqli_real_escape_string($koneksi, $_POST['nim']);
        $nama_mahasiswa = mysqli_real_escape_string($koneksi, $_POST['nama_mahasiswa']);
        $tempat_tgl_lahir = mysqli_real_escape_string($koneksi, $_POST['tempat_tgl_lahir']);
        $jenis_kelamin = mysqli_real_escape_string($koneksi, $_POST['jenis_kelamin']);
        $email = mysqli_real_escape_string($koneksi, $_POST['email']);
        $jurusan = mysqli_real_escape_string($koneksi, $_POST['jurusan']);
        $kurikulum = mysqli_real_escape_string($koneksi, $_POST['kurikulum']);
        $sistem_kuliah = mysqli_real_escape_string($koneksi, $_POST['sistem_kuliah']);

        mysqli_query($koneksi, "UPDATE 'user_form' SET nim = '$nim', nama_mahasiswa = '$nama_mahasiswa', tempat_tgl_lahir = '$tempat_tgl_lahir', jenis_kelamin ='$jenis_kelamin', email = '$email', jurusan = '$jurusan', kurikulum = '$kurikulum', sistem ='$sistem_kuliah'  WHERE id = '$user_name'") or die('query failed');
    }
    ?>

    <div class="bg-modal">
        <div class="modal-content">
            <div class="close">X</div>
            <div class="contentpopup">

                <?php
                $select = mysqli_query($koneksi, "SELECT * FROM `user_form` WHERE id = '$user_name'") or die('query failed');
                if (mysqli_num_rows($select) > 0) {
                    $fetch = mysqli_fetch_assoc($select);
                }
                ?>

                <form action="" method="post" enctype="multipart/form-data">
                    <?php
                    // if ($fetch['image'] == '') {
                    //     echo '<img src="images/default-avatar.png">';
                    // } else {
                    //     echo '<img src="uploaded_img/' . $fetch['image'] . '">';
                    // }
                    // if (isset($message)) {
                    //     foreach ($message as $message) {
                    //         echo '<div class="message">' . $message . '</div>';
                    //     }
                    // }
                    ?>
                    <div class="user-detailspopup">
                        <div class="input-box-popup">
                            <span class="detailspopup">NIM</span>
                            <input type="text" name="nim" value="<?php echo $fetch['nim']; ?>" required>
                        </div>
                        <div class="input-box-popup">
                            <span class="detailspopup">Nama Mahasiswa</span>
                            <input type="text" name="nama_mahasiswa" value="<?php echo $fetch['nama_mahasiswa']; ?>"
                                required>
                        </div>
                        <div class="input-box-popup">
                            <span class="detailspopup">Tempat,Tanggal Lahir</span>
                            <input type="text" name="tempat_tgl_lahir" value="<?php echo $fetch['tempat_tgl_lahir']; ?>"
                                required>
                        </div>
                        <div class="input-box-popup">
                            <span class="detailspopup">Jenis Kelamin</span>
                            <input type="text" name="jenis_kelamin" value="<?php echo $fetch['jenis_kelamin']; ?>"
                                required>
                        </div>
                        <div class="input-box-popup">
                            <span class="detailspopup">Alamat Email</span>
                            <input type="email" name="email" value="<?php echo $fetch['email']; ?>" required>
                        </div>
                        <div class="input-box-popup">
                            <span class="detailspopup">Jurusan</span>
                            <input type="text" name="jurusan" value="<?php echo $fetch['jurusan']; ?>" required>
                        </div>
                        <div class="input-box-popup">
                            <span class="detailspopup">Tahun Ajaran</span>
                            <input type="text" name="kurikulum" value="<?php echo $fetch['kurikulum']; ?>" required>
                        </div>
                        <div class="input-box-popup">
                            <span class="detailspopup">Sistem Kuliah</span>
                            <input type="text" name="sistem_kuliah" value="<?php echo $fetch['sistem_kuliah']; ?>"
                                required>
                        </div>
                        <div>
                            <label>Silakan Kirim Foto Profil</label>
                            <input type="file" name="image" accept="image/jpg, image/jpeg, image/png" class="send">
                        </div>
                    </div>
                    <div class="button">
                        <a href="/edhuniv/media/php/user/daftar-dosen.php"> <input type="submit" value="Simpan"
                                name="submit" class="add"></a>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script>document.getElementById('button').addEventListener('click', function () {
            document.querySelector('.bg-modal').style.display = 'flex';
        });

        document.querySelector('.add').addEventListener('click', function () {
            document.querySelector('.bg-modal').style.display = 'none';
        });

        document.querySelector('.close').addEventListener('click', function () {
            document.querySelector('.bg-modal').style.display = 'none';
        });
    </script>

    <section class="overlay"></section>
    <script src="/edhuniv/media/js/main.js"></script>


</body>

</html>