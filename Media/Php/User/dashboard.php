<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="/edhuniv/media/css/user/dashboard.css" />
</head>

<body>
    <!-- NAVIGASI -->
    <nav>
        <div class="logo">
            <img src="/edhuniv/asset/icon/menu.png" class="menu-icon" />
            <span class="logo-name">Edh University</span>
        </div>
        <div class="container">
            <div class="logo">
                <img src="/edhuniv/asset/icon/menu.png" class="menu-icon" />
                <span class="logo-name">Edh University</span>
            </div>

            <div class="container-menu">
                <ul class="ul-navbar">
                    <li class="li-navbar">
                        <a href="/edhuniv/media/php/user/profil.php" class="navigasi">
                            <img src="/edhuniv/asset/icon/profil.png" class="icon" />
                            <span class="link">Profil</span>
                        </a>
                    </li>
                    <li class="li-navbar">
                        <a href="#" class="navigasi">
                            <img src="/edhuniv/asset/icon/home.png" class="icon" />
                            <span class="link">Dashboard</span>
                        </a>
                    </li>
                    <li class="li-navbar">
                        <a href="/edhuniv/media/php/user/daftar-mahasiswa.php" class="navigasi">
                            <img src="/edhuniv/asset/icon/daftar-mahasiswa.png" class="icon" />
                            <span class="link">Daftar Mahasiswa</span>
                        </a>
                    </li>
                    <li class="li-navbar">
                        <a href="/edhuniv/media/php/user/daftar-dosen.php" class="navigasi">
                            <img src="/edhuniv/asset/icon/daftar-dosen.png" class="icon" />
                            <span class="link">Daftar Dosen</span>
                        </a>
                    </li>
                    <li class="li-navbar">
                        <a href="/edhuniv/media/php/user/jadwal.php" class="navigasi">
                            <img src="/edhuniv/asset/icon/jadwal.png" class="icon" />
                            <span class="link">Jadwal</span>
                        </a>
                    </li>
                </ul>

                <div class="bottom-cotent">
                    <ul class="ul-navbar">
                        <li class="li-navbar">
                            <a href="#" class="navigasi">
                                <img src="/edhuniv/asset/icon/setting.png" class="icon" />
                                <span class="link">Settings</span>
                            </a>
                        </li>
                        <li class="li-navbar">
                            <a href="/edhuniv/media/php/login.php" class="navigasi">
                                <img src="/edhuniv/asset/icon/log-out.png" class="icon" />
                                <span class="link">Logout</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>


    <!-- DASHBOARD -->
    <div class="container">
        <div class="wrapper">
            <div class="card matkul">
                <img src="/edhuniv/asset/jpg/pemrograman-web.jpg" alt="red-shoes" class="pem.web" height="200" width="300">
                <div class="deskripsi">
                    <h3>Prodi S1 T.Informatika</h3>
                    <h4>Pemrograman Web</h4>
                    <ul class="ul-deskripsi">
                        <li>Pengajar</li>
                        <li>Puguh Jayadi</li>
                        <li>0708019402</li>
                    </ul>
                </div>
                <a href="#popup1" class="btn">Buka</a>
            </div>

            <div class="card matkul">
                <img src="/edhuniv/asset/jpg/teknik-simulasi.jpg" alt="red-shoes" class="pem.web" height="200" width="300"
                    display="block">
                <div class="deskripsi">
                    <h3>Prodi S1 T.Informatika</h3>
                    <h4>Teknik Simulasi</h4>
                    <ul class="ul-deskripsi">
                        <li>Pengajar :</li>
                        <li>Moch Yusuf Asyhari</li>
                        <li>0720049601</li>
                    </ul>
                </div>
                <a href="#popup2" class="btn">Buka</a>
            </div>

            <div class="card matkul">
                <img src="/edhuniv/asset/jpg/analisa-desain-sistem.jpg" alt="red-shoes" class="pem.web" height="200"
                    width="300">
                <div class="deskripsi">
                    <h3>Prodi S1 T.Informatika</h3>
                    <h4>Analisa Disain Sistem</h4>
                    <ul class="ul-deskripsi">
                        <li>Pengajar :</li>
                        <li>Pratiwi Susanti</li>
                        <li>0711089301</li>
                    </ul>
                </div>
                <a href="#popup3" class="btn">Buka</a>
            </div>
            <div class="card matkul">
                <img src="/edhuniv/asset/jpg/prak-website.png" alt="red-shoes" class="pem.web" height="200" width="300">
                <div class="deskripsi">
                    <h3>Prodi S1 T.Informatika</h3>
                    <h4>Prak.Website</h4>
                    <ul class="ul-deskripsi">
                        <li>Pengajar :</li>
                        <li>Puguh Jayadi</li>
                        <li>0708019402</li>
                    </ul>
                </div>
                <a href="#popup4" class="btn">Buka</a>
            </div>
            <div class="card matkul">
                <img src="/edhuniv/asset/jpg/kecerdasan-buatan.png" alt="red-shoes" class="pem.web" height="200" width="300"
                    display="block">
                <div class="deskripsi">
                    <h3>Prodi S1 T.Informatika</h3>
                    <h4>Kecerdasan Buatan</h4>
                    <ul class="ul-deskripsi">
                        <li>Pengajar :</li>
                        <li>Kelik Sussolaikah</li>
                        <li>0727048404</li>
                    </ul>
                </div>
                <a href="#popup5" class="btn">Buka</a>
            </div>

            <div class="card matkul">
                <img src="/edhuniv/asset/jpg/rpl.png" alt="red-shoes" class="pem.web" height="200" width="300">
                <div class="deskripsi">
                    <h3>Prodi S1 T.Informatika</h3>
                    <h4>Rekayasa Perangkat Lunak</h4>
                    <ul class="ul-deskripsi">
                        <li>Pengajar :</li>
                        <li>Sri Anardani</li>
                        <li>0726058001</li>
                    </ul>
                </div>
                <a href="#popup6" class="btn">Buka</a>
            </div>
        </div>
    </div>

    <!-- POPUP DESKRIPSI -->
    <div class="popup" id="popup1">
        <div class="popup-deskripsi">
            <div class="popup-jpg">
                <img src="/edhuniv/asset/jpg/popup/web.jpg">
                <img src="/edhuniv/asset/jpg/popup/web2.jpg">
                <img src="/edhuniv/asset/jpg/popup/web3.jpg">

                <a href="#" class="popup-close">&times;</a>
            </div>
            <div class="popup-header">
                <h1>Pemrograman Web</h1>
            </div>
            <div class="popup-text">
                <p> Pemrograman web terbentuk atas 2 kata yaitu pemrograman dan web dimana
                    pemrograman sendiri adalah
                    Proses atau Cara dalam menjalankan sebuah urutan intruksi atau perintah yang diberikan kepada
                    komputer untuk membuat fungsi atau tugas tertentu. dan Web adalah Sistem untuk mengakses,
                    memanipulasi, dan mengunduh dokumen yang terdapat pada komputer yang di hubungkan melalui internet
                    atau jaringan.</p>
            </div>
            <a href="/edhuniv/media/html/content/matkul_1.html" class="btn popup-btn">Buka</a>
        </div>
    </div>

    <div class="popup" id="popup2">
        <div class="popup-deskripsi">
            <div class="popup-jpg">
                <img src="/edhuniv/asset/jpg/popup/simulasi.jpg">
                <img src="/edhuniv/asset/jpg/popup/simulasi2.jpg">
                <img src="/edhuniv/asset/jpg/popup/simulasi3.jpg">

                <a href="#" class="popup-close">&times;</a>
            </div>
            <div class="popup-header">
                <h1>Teknik Simulasi</h1>
            </div>
            <div class="popup-text">
                <p> Simulasi adalah satu-satunya cara yang dapat digunakan untuk mengatasi
                    masalah, jika sistem nyata sulit diamati secara langsung</p>
                <p>Contoh : Jalur penerbangan pesawat ruang angkasa atau satelit.
                    Simulasi juga bisa dilakukan jika solusi analitik tidak bisa dikembangkan,
                    karena sistem sangat kompleks. Di samping itu simulasi juga terpaksa
                    dlakukan jika pengamatan sistem secara langsung tidak dimungkinkan,
                    karena :</p>
            </div>
            <a href="/edhuniv/media/html/content/matkul_2.html" class="btn popup-btn">Buka</a>
        </div>
    </div>

    <div class="popup" id="popup3">
        <div class="popup-deskripsi">
            <div class="popup-jpg">
                <img src="/edhuniv/asset/jpg/popup/analisa.jpg">
                <img src="/edhuniv/asset/jpg/popup/analisa2.jpg">
                <img src="/edhuniv/asset/jpg/popup/analisa3.jpg">

                <a href="#" class="popup-close">&times;</a>
            </div>
            <div class="popup-header">
                <h1>Analisa Desain Sistem</h1>
            </div>
            <div class="popup-text">
                <p>Analisis Sistem atau System Analysis adalah suatu teknik atau metode pemecahan masalah dengan cara
                    menguraikan system ke dalam komponen-komponen pembentuknya untuk mengetahui bagaimana
                    komponen-komponen tersebut bekerja dan saling berinteraksi satu sama lain untuk mencapai tujuan
                    system.</p>
                <p>System Analysis biasanya dilakukan dalam membuat System Design. System Design adalah salah satu
                    langkah dalam teknik pemecahan masalah dimana komponen-komponen pembentuk system digabungkan
                    sehingga membentuk satu kesatuan system yang utuh. Hasil dari System Design merupakan gambaran
                    system yang sudah diperbaiki. Teknik dari System Design ini meliputi proses penambahan,
                    penghilangan, dan pengubahan komponen-komponen dari system semula.</p>
            </div>
            <a href="/edhuniv/media/html/content/matkul_3.html" class="btn popup-btn">Buka</a>
        </div>
    </div>

    <div class="popup" id="popup4">
        <div class="popup-deskripsi">
            <div class="popup-jpg">
                <img src="/edhuniv/asset/jpg/popup/prak.web.jpg">
                <img src="/edhuniv/asset/jpg/popup/prak.web2.jpg">
                <img src="/edhuniv/asset/jpg/popup/prak.web3.jpg">

                <a href="#" class="popup-close">&times;</a>
            </div>
            <div class="popup-header">
                <h1>Praktek Website</h1>
            </div>
            <div class="popup-text">
                <p>Pengembangan web adalah istilah yang luas untuk pekerjaan yang terlibat dalam mengembangkan suatu
                    situs web untuk internet (World Wide Web) atau intranet (jaringan pribadi). Hal ini dapat mencakup
                    desain web, pengembangan konten web, penghubung klien, sisi klien/server-side scripting, web server
                    dan keamanan jaringan konfigurasi, dan e-commerce pembangunan. Namun, kalangan profesional web,
                    “pengembangan web” biasanya merujuk pada aspek non-desain utama dari membangun situs web: menulis
                    markup dan coding . Pengembangan web dapat berkisar dari mengembangkan halaman statis sederhana
                    tunggal teks biasa ke berbasis web yang paling kompleks aplikasi internet, bisnis elektronik, atau
                    layanan jaringan sosial.</p>
                <p>Untuk organisasi yang lebih besar dan bisnis, tim pengembangan web dapat terdiri dari ratusan orang
                    (web developer). Organisasi yang lebih kecil mungkin hanya memerlukan permanen atau kontrak tunggal
                    webmaster, atau tugas sekunder untuk posisi pekerjaan yang terkait seperti desainer grafis dan/atau
                    sistem informasi teknisi. Pengembangan web mungkin merupakan upaya kolaborasi antar departemen bukan
                    domain dari sebuah departemen yang ditunjuk.</p>
            </div>
            <a href="/edhuniv/media/html/content/matkul_4.html" class="btn popup-btn">Buka</a>
        </div>
    </div>

    <div class="popup" id="popup5">
        <div class="popup-deskripsi">
            <div class="popup-jpg">
                <img src="/edhuniv/asset/jpg/popup/kecerdasan2.jpg">
                <img src="/edhuniv/asset/jpg/popup/kecerdasan.jpg">
                <img src="/edhuniv/asset/jpg/popup/kecerdasan3.jpg">

                <a href="#" class="popup-close">&times;</a>
            </div>
            <div class="popup-header">
                <h1>Kecerdasan Buatan</h1>
            </div>
            <div class="popup-text">
                <p>Kecerdasan buatan atau Artificial Intelligence (AI) adalah simulasi dari kecerdasan yang dimiliki
                    oleh manusia yang dimodelkan di dalam mesin dan diprogram agar bisa berpikir seperti halnya manusia.
                    Sedangkan menurut Mc Leod dan Schell, kecerdasan buatan adalah aktivitas penyediaan mesin seperti
                    komputer dengan kemampuan untuk menampilkan perilaku yang dianggap sama cerdasnya dengan jika
                    kemampuan tersebut ditampilkan oleh manusia.</p>
                <p>Dengan kata lain AI merupakan sistem komputer yang bisa melakukan pekerjaan-pekerjaan yang umumnya
                    memerlukan tenaga manusia atau kecerdasan manusia untuk menyelesaikan pekerjaan tersebut.</p>
            </div>
            <a href="/edhuniv/media/html/content/matkul_5.html" class="btn popup-btn">Buka</a>
        </div>
    </div>

    <div class="popup" id="popup6">
        <div class="popup-deskripsi">
            <div class="popup-jpg">
                <img src="/edhuniv/asset/jpg/popup/perangkat-lunak3.jpg">
                <img src="/edhuniv/asset/jpg/popup/perangkat-lunak2.jpg">
                <img src="/edhuniv/asset/jpg/popup/perangkat-lunak.jpg">

                <a href="#" class="popup-close">&times;</a>
            </div>
            <div class="popup-header">
                <h1>Rekayasa Perangkat Lunak</h1>
            </div>
            <div class="popup-text">
                <p>Rekayasa Perangkat Lunak (RPL, atau dalam bahasa Inggris: Software Engineering atau SE) adalah satu
                    bidang profesi yang mendalami cara-cara pengembangan perangkat lunak termasuk pembuatan,
                    pemeliharaan, manajemen organisasi pengembangan perangkat lunak dan manajemen kualitas.</p>
                <p>EEE Computer Society mendefinisikan rekayasa perangkat lunak sebagai penerapan suatu pendekatan yang
                    sistematis, disiplin dan terkuantifikasi atas pengembangan, penggunaan dan pemeliharaan perangkat
                    lunak, serta studi atas pendekatan-pendekatan ini, yaitu penerapan pendekatan engineering atas
                    perangkat lunak.</p>
                <p>Rekayasa perangkat lunak adalah pengubahan perangkat lunak itu sendiri guna mengembangkan,
                    memelihara, dan membangun kembali dengan menggunakan prinsip rekayasa untuk menghasilkan perangkat
                    lunak yang dapat bekerja lebih efisien dan efektif untuk pengguna.</p>
            </div>
            <a href="/edhuniv/media/html/content/matkul_6.html" class="btn popup-btn">Buka</a>
        </div>
    </div>

    <section class="overlay"></section>
    <script src="/edhuniv/media/js/main.js"></script>

</body>

</html>